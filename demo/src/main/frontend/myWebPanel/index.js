export const mount = elem => {
    console.log('mount');

    elem.innerHTML = '<h2>Hello my plugin!</h2>';
};

export const unmount = elem => {
    elem.innerHTML = '';
    console.log('unmount');
};
