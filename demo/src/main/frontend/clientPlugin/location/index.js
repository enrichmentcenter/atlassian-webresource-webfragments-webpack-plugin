import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { ClientLocation, ClientWebfragment } from '@atlassian/atlassian-w2-react';
import 'wr-dependency!com.atlassian.plugins.atlassian-plugins-frontend-api:atlassian-plugins-frontend-api-resources';

class ClientPluginLocation extends React.Component {
    state = {
        context: {
            name: 'first name',
        },
    }

    renderItem(id) {
        const context = {
            myData: 'test'
        };

        return (
            <ClientLocation key={id} name="my-location" context={this.state.context}>
                {plugins => (
                    <div style={{padding: '20px 0'}}>
                        <h1>Hello my client location #{id}</h1>
                        {plugins.map(plugin => <ClientWebfragment key={plugin.id} descriptor={plugin} context={{...context, id}} />)}
                    </div>
                )}
            </ClientLocation>
        )
    }

    render() {
        const ids = [1, 147, 12];

        return (
            <div>
                {ids.map(id => this.renderItem(id))}
            </div>
        );
    }
}

export const mount = elm => {
    render(<ClientPluginLocation />, elm);
};

export const unmount = elm => {
    unmountComponentAtNode(elm);
};
