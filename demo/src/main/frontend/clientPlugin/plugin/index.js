import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';

export const PLUGIN_ID = 'my-client-plugin';

function MyClientPlugin({ context }) {
    return (
        <div style={{padding: '20px 0'}}>
            <h2>Plugin: <b>{PLUGIN_ID}</b></h2>
            <p>
                Context: {JSON.stringify(context)}
            </p>
        </div>
    );
}

export function mount(element, context) {
    render(<MyClientPlugin context={context} />, element);
}

export function unmount(element) {
    unmountComponentAtNode(element);
}
