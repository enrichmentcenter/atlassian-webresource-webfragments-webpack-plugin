package com.atlassian.plugins.jira.api;

public interface MyPluginComponent
{
    String getName();
}