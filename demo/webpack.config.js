const path = require('path');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const BuildReportPlugin = require('@atlassian/atlassian-wrm-build-report-plugin');
const WebfragmentsPlugin = require('../dist/WebfragmentsPlugin');

const webfragmentsPlugin = new WebfragmentsPlugin();

const OUTPUT_PATH = path.join(__dirname, 'target', 'classes');

module.exports = {
    mode: 'development',

    entry: {
        ...webfragmentsPlugin.generateEntrypoints(),
    },

    output: {
        path: OUTPUT_PATH,
        filename: '[name].[chunkhash].js',
        chunkFilename: '[name].[chunkhash].js',
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            presets: ['@babel/preset-env', '@babel/preset-react'],
                            plugins: ['transform-class-properties'],
                        },
                    },
                ],
            },
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader',
                options: {
                    errorsAsWarnings: true,
                },
            },
        ],
    },

    plugins: [
        new WrmPlugin({
            pluginKey: 'com.atlassian.plugins.jira.webfragments-playground',
            xmlDescriptors: path.resolve(OUTPUT_PATH, 'META-INF', 'plugin-descriptors', 'wr-defs.xml'),
            contextMap: {
                ...webfragmentsPlugin.generateContextMap(),
            },
        }),
        new BuildReportPlugin({
            filename: './report.json',
            staticReport: true,
            baseUrl: 'http://localhost:2990/jira'
        }),
        webfragmentsPlugin,
    ],

    resolve: {
        alias: {
            '@lib': path.resolve(__dirname, '../dist'),
        },
    },
};
