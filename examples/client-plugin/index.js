import React, { useEffect } from "react";
import { render, unmountComponentAtNode } from "react-dom";

export const PLUGIN_ID = "my-plugin-1";

function ClientPlugin1({ context }) {
  useEffect(() => {
    function eventListener() {
      console.log(`Event Listener from ${PLUGIN_ID} for val ${context.val}`);
    }

    document.addEventListener("my-event", eventListener);

    return function cleanUp() {
      document.removeEventListener("my-event", eventListener);
    };
  });

  return (
    <div>
      <p>
        Plugin: <b>{PLUGIN_ID}</b>
      </p>
      <p>
        Instance of Val: <b>{context.val}</b>
      </p>
      <p>
        <button onClick={() => alert(context.greeting)}>Say Hi!</button>
        <button onClick={() => alert(context.farewell)}>Say Bye!</button>
      </p>
    </div>
  );
}

export function mount(element, context) {
  console.log("mounting", { id: PLUGIN_ID, element, context });

  render(<ClientPlugin1 context={context} />, element);
}

export function unmount(element) {
  console.log("unmounting", { id: PLUGIN_ID, element });

  unmountComponentAtNode(element);
}
