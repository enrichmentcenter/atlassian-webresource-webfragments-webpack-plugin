const path = require('path');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const WebfragmentsPlugin = require('../dist/WebfragmentsPlugin');

const webfragmentsPlugin = new WebfragmentsPlugin({
    cwd: __dirname,
});

const OUTPUT_PATH = path.join(__dirname, 'target', 'classes');

module.exports = {
    mode: 'development',

    entry: {
        ...webfragmentsPlugin.generateEntrypoints(),
    },

    output: {
        path: OUTPUT_PATH,
        filename: '[name].[chunkhash].js',
        chunkFilename: '[name].[chunkhash].js',
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            presets: ['@babel/preset-env', '@babel/preset-react'],
                            plugins: ['transform-class-properties'],
                        },
                    },
                ],
            },
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader',
                options: {
                    errorsAsWarnings: true,
                },
            },
        ],
    },

    plugins: [
        new WrmPlugin({
            pluginKey: 'com.atlassian.plugins.jira.webfragments-playground',
            xmlDescriptors: path.resolve(OUTPUT_PATH, 'META-INF', 'plugin-descriptors', 'wr-defs.xml'),
            contextMap: {
                ...webfragmentsPlugin.generateContextMap(),
            },
        }),
        webfragmentsPlugin,
    ],
};
