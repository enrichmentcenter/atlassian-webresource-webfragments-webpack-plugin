import glob from 'glob';
import kebabCase from 'lodash.kebabcase';

const DEFAULT_MANIFEST_PATTERN = '**/manifest.json';
const MANIFESTS_TMP = new Map();

interface IFindEntryPointsOptions {
    cwd?: string;
    pattern?: string;
    filter?: {
        [filterKey: string]: string | number | RegExp;
    };
}

const readManifestContent = (manifest: FilePath) => {
    if (!MANIFESTS_TMP.has(manifest)) {
        MANIFESTS_TMP.set(manifest, require(manifest));
    }

    return MANIFESTS_TMP.get(manifest);
};

export const findManifests = (options: IFindEntryPointsOptions = {}): Array<IManifestDescriptor> => {
    const { pattern, cwd, filter = {} } = options;
    const filterKeys = Object.keys(filter);

    const manifests = glob.sync(pattern || DEFAULT_MANIFEST_PATTERN, { absolute: true, cwd: cwd || process.cwd() });

    return manifests
        .filter(manifest => {
            if (filterKeys.length > 0) {
                const manifestContent = readManifestContent(manifest);

                return filterKeys.every(filterKey => {
                    if (!manifestContent[filterKey]) {
                        return false;
                    }

                    if (filter[filterKey] instanceof RegExp) {
                        return (filter[filterKey] as RegExp).test(manifestContent[filterKey]);
                    }

                    return manifestContent[filterKey] === filter[filterKey];
                });
            }

            return true;
        })
        .map(file => {
            return {
                file,
                manifest: readManifestContent(file),
            };
        });
};

export function getIdForManifest(manifest: IManifestPayload) {
    return kebabCase(manifest.key);
}

export function getFilenameForEntrypoint(entrypointName: string) {
    return `./generated-webfragments/${entrypointName}.js`;
}
