declare module 'virtual-module-webpack-plugin' {
    interface IVirtualModuleParams {
        moduleName: string;
        contents: string;
    }

    export default class VirtualModuleWebpackPlugin {
        constructor(params: IVirtualModuleParams);

        apply(compiler: any): void;
    }
}
