type RegisterCallbackShape = (element: HTMLElement, contextProviderData?: any) => void;

// Module import in JS Template. Will be replaced by Plugin when building.
declare module '<%= entryPath %>' {
    export const mount: (element: HTMLElement, resourceData?: any) => void;
    export const unmount: (element: HTMLElement) => void;
}

declare module '@atlassian/atlassian-webresource-webpanel-webpack-plugin' {
    export interface EntryPointInstance {
        mount(element: HTMLElement, resourceData?: any): void;
        unmount(element: HTMLElement): void;
    }

    type WebFragmentId = string;
    type DOMSelector = string;
    type FilePath = string;

    enum LoadStyles {
        sync = 'sync',
        async = 'async',
        ondDemand = 'on-demand',
    }

    /**
     * @param id required
     * @param element required
     * @param type {default = 'sync'}
     */
    interface IPushMethodDescriptor {
        id: WebFragmentId;
        element: HTMLElement | Array<HTMLElement> | DOMSelector;
        type?: LoadStyles;
        resource?: string;
        payload?: any;
    }

    export default class WRMWebfragmentRegistry {
        push(data: IPushMethodDescriptor): void;

        registerCallback(webFragmentId: WebFragmentId, callback: RegisterCallbackShape): void;
    }

    enum ManifestTypes {
        webPanel = 'web-panel',
        webItem = 'web-item',
        webSection = 'web-section',
        webWork = 'web-work',
        xWork = 'x-work',
    }

    enum ConditionsTypes {
        or = 'OR',
        and = 'AND',
    }

    interface IConditionDescriptor {
        class: string;
    }

    interface IConditionsDescriptor {
        type: ConditionsTypes;
        conditions: Array<IConditionDescriptor>;
    }

    export interface BaseManifestDescriptor {
        type: ManifestTypes;
        key: WebFragmentId;
        loadStyle: LoadStyles;
        loadLabel: string;
        entry: FilePath;
        contextProvider?: string;
        conditions?: IConditionsDescriptor;
    }

    export interface IWebPanelManifest extends BaseManifestDescriptor {
        label: string;
        location: string;
        weight?: number;
    }

    export interface IWebSectionManifest extends BaseManifestDescriptor {}
}
