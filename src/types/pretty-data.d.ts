declare module 'pretty-data' {
    interface IPrettyData {
        xml(rawXmlString: string): string;
    }

    export const pd: IPrettyData;
}
