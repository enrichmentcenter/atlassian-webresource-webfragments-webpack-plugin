declare module 'atlassian-webresource-webpack-plugin' {
    interface IWebresourceWebpackPluginOptions {
        pluginKey: string;
        xmlDescriptors: string;
    }

    export default class WrmPlugin {
        constructor(options: IWebresourceWebpackPluginOptions);
    }
}

declare module 'atlassian-webresource-webpack-plugin/src/helpers/xml' {
    type IRenderElement = (elementName: string, attributes: object, children?: string | Array<string>) => string;
    export const renderElement: IRenderElement;
}

declare module 'atlassian-webresource-webpack-plugin/src/renderCondition' {
    export default function(conditions: object | undefined): string;
}
