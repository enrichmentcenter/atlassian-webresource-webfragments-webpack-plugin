interface Window {
    __webpanel_register: Array<IWebPanelPayloadDescriptor> | any;
    __atlassian_plugins_api: any;
}

declare const WRM: {
    require: (resource: string | Array<string>) => Promise<any>;
};

interface IWebPanelPayloadDescriptor {
    id: string;
    resources: string;
    element: HTMLElement;
    type: LoadStyle;
    payload: any;
}

type FilePath = string;

type LoadStyle = 'sync' | 'async' | 'on-demand';

interface IAssetDescriptor {
    type: string;
    path: FilePath;
    content: string;
}

interface IManifestDescriptor {
    file: string;
    manifest: IManifestPayload;
}

interface IManifestPayload {
    type: string;
    key: string;
    entry: string;
    name?: string;
    label: string;
    weight?: number;
    location: string;
    contextProvider: string;
    conditions?: object;
    loadStyle?: LoadStyle;
    loadLabel?: string;
    actionName?: string;
    alias?: string;
    items: Array<IWebItemOptions>;
    context?: string;
}

interface IWebItemOptions {
    key: string;
    name: string;
    link: string;
    location: string;
    weight: number;
    label?: string;
}

interface IWebfragmentsOptions {
    manifest: IManifestPayload;
    manifestPath: string;
    pluginKey: string;
}

interface IWrmPluginOptions {
    pluginKey: string;
    contextMap: object;
    conditionMap: object;
    transformationMap: object;
    webresourceKeyMap: object;
    providedDependencies: object;
    xmlDescriptors: string;
    assetContentTypes: string;
    locationPrefix: string;
    watch: boolean;
    watchPrepare: boolean;
    noWRM: boolean;
    verbose: boolean;
}
