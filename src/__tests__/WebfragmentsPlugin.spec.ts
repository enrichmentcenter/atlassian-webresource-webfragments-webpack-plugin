import WrmPlugin from 'atlassian-webresource-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import fs from 'fs';
import glob from 'glob';
import path from 'path';
import webpack from 'webpack';

import WebfragmentsPlugin from '../WebfragmentsPlugin';

describe('WebfragmentsPlugin', () => {
    test('default compilation', (done: () => void) => {
        const webfragmentsPlugin = new WebfragmentsPlugin({
            cwd: path.join(__dirname, '..', '..', 'examples'),
        });

        const config = {
            entry: {
                ...webfragmentsPlugin.generateEntrypoints(),
            },

            output: {
                path: path.join(__dirname, 'tmp', 'my-plugin'),
            },

            module: {
                rules: [
                    {
                        test: /\.js$/,
                        exclude: /(node_modules)/,
                        use: [
                            {
                                loader: 'babel-loader',
                                options: {
                                    cacheDirectory: true,
                                    presets: ['@babel/preset-env', '@babel/preset-react'],
                                    plugins: ['transform-class-properties'],
                                },
                            },
                        ],
                    },
                    {
                        test: /\.tsx?$/,
                        loader: 'awesome-typescript-loader',
                        options: {
                            errorsAsWarnings: true,
                        },
                    },
                ],
            },

            plugins: [
                new CleanWebpackPlugin(),
                new WrmPlugin({
                    pluginKey: 'com.atlassian.plugins.jira.webfragments-playground',
                    xmlDescriptors: path.resolve(
                        __dirname,
                        'tmp',
                        'my-plugin',
                        'META-INF',
                        'plugin-descriptors',
                        'wr-defs.xml'
                    ),
                }),
                webfragmentsPlugin,
            ],
        } as webpack.Configuration;

        webpack(config, (err: Error, stats: webpack.Stats) => {
            expect(err).toBe(null);
            expect(stats.hasErrors()).toBe(false);

            glob(`${__dirname}/tmp/my-plugin/**/*`, (err: Error, generatedResult: Array<string>) => {
                const generatedFiles = generatedResult
                    .filter(generatedFile => fs.statSync(generatedFile).isFile())
                    .map(generatedFile => generatedFile.replace(__dirname, './src/__tests__'));

                expect(err).toBe(null);

                expect(generatedFiles).toMatchSnapshot();

                for (const generatedFile of generatedFiles) {
                    const generatedFileContent = fs.readFileSync(generatedFile, 'utf-8');

                    expect(generatedFileContent).toMatchSnapshot();
                }

                done();
            });
        });
    });

    test('with empty web-fragments list', (done: () => void) => {
        const webfragmentsPlugin = new WebfragmentsPlugin({
            cwd: path.join(__dirname, '..', '..', 'fake-dir'),
        });

        const targetDir = path.join(__dirname, 'tmp', 'fake-plugin');

        const config = {
            entry: {
                main: path.join(__dirname, '__mocks__', 'fake-entrypoint.js'),
            },

            output: {
                path: targetDir,
            },

            plugins: [
                new CleanWebpackPlugin(),
                new WrmPlugin({
                    pluginKey: 'com.atlassian.plugins.jira.webfragments-playground',
                    xmlDescriptors: path.resolve(targetDir, 'META-INF', 'plugin-descriptors', 'wr-defs.xml'),
                }),
                webfragmentsPlugin,
            ],
        } as webpack.Configuration;

        webpack(config, (err: Error, stats: webpack.Stats) => {
            expect(err).toBe(null);
            expect(stats.hasErrors()).toBe(false);

            expect(
                fs.existsSync(path.join(targetDir, 'META-INF', 'plugin-descriptors', 'wr-generated-webfragments.xml'))
            ).toBe(false);

            done();
        });
    });
});
