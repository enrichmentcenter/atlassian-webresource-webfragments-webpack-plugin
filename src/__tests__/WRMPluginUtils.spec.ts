import path from 'path';

import * as WrmPluginUtils from '../WRMPluginUtils';

interface IManifestDescriptor {
    file: string;
    manifest: {
        type: string;
        key: string;
        entry: string;
        name?: string;
        label: string;
        weight?: number;
        location: string;
        contextProvider: string;
        conditions?: object;
        loadStyle?: string;
        loadLabel?: string;
        actionName?: string;
        alias?: string;
        items: Array<any>;
        context?: string;
    };
}

const replaceAbsolutePathByRelative = (manifest: IManifestDescriptor) => {
    return {
        ...manifest,
        file: manifest.file.replace(process.cwd(), '.'),
    };
};

describe('WrmPluginUtils', () => {
    test('"findManifests" default behavior', () => {
        const manifests = WrmPluginUtils.findManifests().map(replaceAbsolutePathByRelative);

        expect(manifests).toEqual([
            {
                file: './demo/src/main/frontend/clientPlugin/another-plugin/manifest.json',
                manifest: {
                    type: 'client-plugin',
                    key: 'my-client-plugin-2',
                    entry: './index.js',
                    location: 'my-location',
                    context: 'jira.view.issue',
                    weight: 10,
                },
            },
            {
                file: './demo/src/main/frontend/clientPlugin/location/manifest.json',
                manifest: {
                    type: 'web-panel',
                    key: 'client-plugin-location',
                    loadStyle: 'sync',
                    loadLabel: 'Client Plugin Location Webpanel',
                    entry: './index.js',
                    label: 'My Client Plugin Location',
                    weight: 1,
                    location: 'atl.jira.view.issue.right.context',
                },
            },
            {
                file: './demo/src/main/frontend/clientPlugin/plugin/manifest.json',
                manifest: {
                    type: 'client-plugin',
                    key: 'my-client-plugin',
                    entry: './index.js',
                    location: 'my-location',
                    context: 'jira.view.issue',
                    weight: 1,
                },
            },
            {
                file: './demo/src/main/frontend/myWebPanel/manifest.json',
                manifest: {
                    type: 'web-panel',
                    key: 'my-web-panel',
                    loadStyle: 'sync',
                    loadLabel: 'Load Webpanel data',
                    entry: './index.js',
                    label: 'jira.webpanel.title',
                    weight: 1,
                    location: 'atl.jira.view.issue.right.context',
                },
            },
            {
                file: './examples/client-plugin/manifest.json',
                manifest: {
                    type: 'client-plugin',
                    key: 'my-plugin-1',
                    entry: './index.js',
                    location: 'my-location',
                    context: 'jira.view.issue',
                },
            },
            {
                file: './examples/web-panel/manifest.json',
                manifest: {
                    type: 'web-panel',
                    key: 'example-web-panel',
                    loadStyle: 'sync',
                    loadLabel: 'Load Webpanel data',
                    entry: './index.js',
                    label: 'jira.webpanel.title',
                    weight: 1,
                    location: 'atl.jira.view.issue.right.context',
                },
            },
        ]);
    });

    test('"findManifests" with specific cwd', () => {
        const manifests = WrmPluginUtils.findManifests({
            cwd: path.join(process.cwd(), 'examples'),
        }).map(replaceAbsolutePathByRelative);

        expect(manifests).toEqual([
            {
                file: './examples/client-plugin/manifest.json',
                manifest: {
                    type: 'client-plugin',
                    key: 'my-plugin-1',
                    entry: './index.js',
                    location: 'my-location',
                    context: 'jira.view.issue',
                },
            },
            {
                file: './examples/web-panel/manifest.json',
                manifest: {
                    type: 'web-panel',
                    key: 'example-web-panel',
                    loadStyle: 'sync',
                    loadLabel: 'Load Webpanel data',
                    entry: './index.js',
                    label: 'jira.webpanel.title',
                    weight: 1,
                    location: 'atl.jira.view.issue.right.context',
                },
            },
        ]);
    });

    test('"findManifests" with filter', () => {
        const manifests = WrmPluginUtils.findManifests({
            filter: {
                key: 'example-web-panel',
            },
        }).map(replaceAbsolutePathByRelative);

        expect(manifests).toEqual([
            {
                file: './examples/web-panel/manifest.json',
                manifest: {
                    entry: './index.js',
                    key: 'example-web-panel',
                    label: 'jira.webpanel.title',
                    loadLabel: 'Load Webpanel data',
                    loadStyle: 'sync',
                    location: 'atl.jira.view.issue.right.context',
                    type: 'web-panel',
                    weight: 1,
                },
            },
        ]);
    });
});
