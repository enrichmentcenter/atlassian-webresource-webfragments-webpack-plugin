import IBaseGenerator from './base.generator';
import WebPanelsGenerator from './web-panels/web-panels.generator';
import ClientPluginsGenerator from './client-plugins/client-plugins.generator';

const GENERATORS = {
    'web-panel': new WebPanelsGenerator(),
    'client-plugin': new ClientPluginsGenerator(),
} as {
    [type: string]: IBaseGenerator;
};

export const getGenerator = (type: string) => {
    if (!GENERATORS[type]) {
        throw new Error(`Can not find generator for ${type}`);
    }

    return GENERATORS[type];
};
