import { IVirtualModuleParams } from 'virtual-module-webpack-plugin';
export interface IEntrypointDef {
    [id: string]: string;
}

export interface IContextMap {
    [id: string]: string | undefined;
}

export enum AssetsTypes {
    velocity = 'velocity',
}

export interface IWebfragmentAsset {
    type: AssetsTypes;
    path: string;
    content: string;
}

export default interface IBaseGenerator {
    getEntrypointsDefinition(manifest: IManifestPayload): IEntrypointDef;

    getContextMap(manifest: IManifestPayload): IContextMap;

    getEntrypointsContent(webfragmentOptions: IWebfragmentsOptions): Array<IVirtualModuleParams>;

    getWebfragmentsXMLDescriptor(
        webfragemtnOptions: IWebfragmentsOptions,
        webfragmentAssets: Array<IWebfragmentAsset>
    ): string;

    getWebfragmentsAssets(webfragemtnOptions: IWebfragmentsOptions): Array<IWebfragmentAsset>;

    combineWebfragmentsXMLDescriptors(descriptors: Array<string>): string;
}
