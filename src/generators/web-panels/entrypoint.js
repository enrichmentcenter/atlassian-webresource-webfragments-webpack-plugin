import 'wr-dependency!com.atlassian.plugins.atlassian-plugins-frontend-api:atlassian-plugins-frontend-api-resources';
import { mount, unmount } from '<%= entryPath %>';

const { registry } = window.__atlassian_plugins_api;

registry.onWebfragmentReady('<%= webfragmentId %>', (element, context) => mount(element, context));
registry.onWebfragmentDestroy('<%= webfragmentId %>', element => unmount(element));

if (module.hot) {
    module.hot.accept('<%= entryPath %>', () => {
        try {
            const mount = require('<%= entryPath %>').mount;
            registry.onWebfragmentReady('<%= webfragmentId %>', (element, context) =>
                mount(element, context)
            );
        } catch (e) {
            console.error(e);
        }
    });
}
