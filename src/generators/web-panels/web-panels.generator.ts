import fs from 'fs';
import path from 'path';
import template from 'lodash.template';

import { renderElement } from 'atlassian-webresource-webpack-plugin/src/helpers/xml';
import renderCondition from 'atlassian-webresource-webpack-plugin/src/renderCondition';

import * as WrmPluginUtils from '../../WRMPluginUtils';
import IBaseGenerator, { AssetsTypes, IEntrypointDef, IWebfragmentAsset } from '../base.generator';

interface ITemplateParams {
    webfragmentId: string;
    manifestPath: string;
    entryPath: string;
    resourceKey: string;
    loadStyle: string;
    options: {
        [key: string]: string | undefined;
    };
}

export default class WebPanelGenerator implements IBaseGenerator {
    getEntrypointsDefinition(manifest: IManifestPayload): IEntrypointDef {
        const id = WrmPluginUtils.getIdForManifest(manifest);

        return {
            // Main Entrypoint module nane and filename
            [id]: WrmPluginUtils.getFilenameForEntrypoint(id),
        };
    }

    getContextMap() {
        // Opt Out of this method. We don't need to override the default context for any web-resource
        return {};
    }

    getEntrypointsContent(webfragmentOptions: IWebfragmentsOptions) {
        const [mainEntrypointModule] = Object.values(this.getEntrypointsDefinition(webfragmentOptions.manifest));

        return [
            {
                moduleName: mainEntrypointModule,
                contents: this.getMainEntrypointContent(webfragmentOptions),
            },
        ];
    }

    getWebfragmentsXMLDescriptor(webfragemtnOptions: IWebfragmentsOptions, webfragmentAssets: IWebfragmentAsset[]) {
        const { manifest } = webfragemtnOptions;
        const velocityResources = webfragmentAssets
            .filter(_asset => _asset.type === AssetsTypes.velocity)
            .map(velocityTemplate =>
                renderElement('resource', { type: 'velocity', name: 'view', location: velocityTemplate.path })
            );

        if (velocityResources.length > 1) {
            throw new Error('You can only specify one view for a web-panel.');
        }

        const contextProvider = manifest.contextProvider
            ? renderElement('context-provider', { class: manifest.contextProvider })
            : '';

        return renderElement(
            'web-panel',
            {
                key: manifest.key,
                location: manifest.location,
                weight: manifest.weight,
            },
            [
                renderElement('label', { key: manifest.label }),
                renderCondition(manifest.conditions),
                contextProvider,
                ...velocityResources,
            ]
        );
    }

    getWebfragmentsAssets(webfragmentOptions: IWebfragmentsOptions) {
        const assets = [];
        const templateParams = this.getTemplateParams(webfragmentOptions);
        const templateContent = this.getTemplateContent(templateParams);

        assets.push({
            type: AssetsTypes.velocity,
            path: `generated-template-${templateParams.webfragmentId}.vm`,
            content: templateContent,
        });

        return assets;
    }

    private getMainEntrypointContent(webfragmentOptions: IWebfragmentsOptions) {
        const rawEntrypointContent = fs.readFileSync(path.join(__dirname, 'entrypoint.js'), 'utf8');

        const templateParams = this.getTemplateParams(webfragmentOptions);

        return template(rawEntrypointContent)(templateParams);
    }

    private getTemplateContent(params: ITemplateParams): string {
        const templateFolder = path.join(__dirname, '..', '..', '..', 'src', 'generators', 'web-panels', 'templates');
        const { webfragmentId, resourceKey, options } = params;
        const { loadType } = options;

        let templateName = 'base.vm';

        if (loadType === 'async') {
            templateName = 'async.vm';
        }

        if (loadType === 'on-deman') {
            templateName = 'on-demand.vm';
        }

        const templatePath = path.join(templateFolder, templateName);
        const rawTemplateContent = fs.readFileSync(templatePath, 'utf-8');

        const templateOptions = {
            webfragmentId,
            resourceKey,
        };

        return template(rawTemplateContent)(templateOptions);
    }

    private getTemplateParams(webfragemtnOptions: IWebfragmentsOptions): ITemplateParams {
        const { manifest, manifestPath, pluginKey } = webfragemtnOptions;
        const webfragmentId = WrmPluginUtils.getIdForManifest(manifest);

        return {
            webfragmentId,
            manifestPath,
            entryPath: path.resolve(path.dirname(manifestPath), manifest.entry),
            resourceKey: `${pluginKey}:entrypoint-${webfragmentId}`,
            loadStyle: this.isCorrectLoadStyle(manifest.loadStyle) ? manifest.loadStyle : 'sync',
            options: {
                loadLabel: manifest.loadLabel,
                loadStyle: manifest.loadStyle,
            },
        };
    }

    private isCorrectLoadStyle(loadStyle: LoadStyle | undefined): loadStyle is LoadStyle {
        return loadStyle === 'sync' || loadStyle === 'async' || loadStyle === 'on-demand';
    }

    combineWebfragmentsXMLDescriptors(descriptors: Array<string>) {
        return `<webpanels>${descriptors.join('\n')}</webpanels>`;
    }
}
