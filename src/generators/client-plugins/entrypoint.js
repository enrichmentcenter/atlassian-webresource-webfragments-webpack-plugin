import 'wr-dependency!com.atlassian.plugins.atlassian-plugins-frontend-api:atlassian-plugins-frontend-api-resources';
import { mount, unmount } from '<%= entryPath %>';

const { ClientWebfragment, registry } = window.__atlassian_plugins_api;

const clientWebFragment = new ClientWebfragment({
    id: '<%= webfragmentId %>',
    label: '<%= webfragmentLabel %>',
    location: '<%= location %>',
    weight: '<%= weight %>',
});

/** HOOK TO EVENTS TO RENDER/DESTROY */
clientWebFragment.onReady((element, context) => mount(element, context));
clientWebFragment.onDestroy((element) => unmount(element));

/** REGISTER PLUGIN */
registry.register(clientWebFragment);

export default {};
