import fs from 'fs';
import path from 'path';
import template from 'lodash.template';

import * as WrmPluginUtils from '../../WRMPluginUtils';
import IBaseGenerator, { IWebfragmentAsset } from '../base.generator';

interface ITemplateParams {
    webfragmentId: string;
    webfragmentLabel: string;
    resourceKey: string;
    entryPath: string;
    location: string;
    weight: number | undefined;
}

export default class ClientPluginsGenerator implements IBaseGenerator {
    getEntrypointsDefinition(manifest: IManifestPayload) {
        const id = WrmPluginUtils.getIdForManifest(manifest);

        return {
            [id]: WrmPluginUtils.getFilenameForEntrypoint(id),
        };
    }

    getContextMap(manifest: IManifestPayload) {
        const [mainEntrypointId] = Object.keys(this.getEntrypointsDefinition(manifest));

        // We will load the main entrypoint using the location name as its `context`. The Client API will handle when to load them.
        return {
            [mainEntrypointId]: manifest.location,
        };
    }

    getEntrypointsContent(webfragmentOptions: IWebfragmentsOptions) {
        const [mainEntrypointModule] = Object.values(this.getEntrypointsDefinition(webfragmentOptions.manifest));

        const mainEntrypointContent = this.getMainEntrypointContent(webfragmentOptions);

        return [
            {
                moduleName: mainEntrypointModule,
                contents: mainEntrypointContent,
            },
        ];
    }

    // TODO: find a better way to opt-out of these methods

    getWebfragmentsXMLDescriptor(): string {
        // No Assets required
        return '';
    }

    getWebfragmentsAssets(): IWebfragmentAsset[] {
        // No Assets required
        return [];
    }

    combineWebfragmentsXMLDescriptors(): string {
        // No Assets required
        return '';
    }

    private getMainEntrypointContent(webfragmentOptions: IWebfragmentsOptions) {
        const rawMainEntrypointContent = fs.readFileSync(path.join(__dirname, 'entrypoint.js'), 'utf8');

        const templateParams = this.getTemplateParams(webfragmentOptions);

        return template(rawMainEntrypointContent)(templateParams);
    }

    private getTemplateParams(webfragemtnOptions: IWebfragmentsOptions): ITemplateParams {
        const { manifest, manifestPath, pluginKey } = webfragemtnOptions;
        const webfragmentId = WrmPluginUtils.getIdForManifest(manifest);

        return {
            webfragmentId,
            webfragmentLabel: manifest.label,
            entryPath: path.resolve(path.dirname(manifestPath), manifest.entry),
            resourceKey: `${pluginKey}:entrypoint-${webfragmentId}`,
            location: manifest.location,
            weight: manifest.weight,
        };
    }
}
