import path from 'path';

import { Compiler, Output, Plugin } from 'webpack';

import { pd as PrettyData } from 'pretty-data';
import VirtualModulePlugin, { IVirtualModuleParams } from 'virtual-module-webpack-plugin';

import { getGenerator } from './generators';
import * as WrmPluginUtils from './WRMPluginUtils';
import { IWebfragmentAsset } from './generators/base.generator';

interface IWebfragmentsPluginOptions {
    pattern?: string;
    cwd?: string;
    filter?: {
        [filterKey: string]: any;
    };
}

export = class WebfragmentsPlugin {
    private manifests: Array<IManifestDescriptor>;

    constructor(options: IWebfragmentsPluginOptions = {}) {
        this.manifests = WrmPluginUtils.findManifests({
            cwd: options.cwd,
            filter: options.filter,
            pattern: options.pattern,
        });
    }

    public generateEntrypoints() {
        return this.manifests.reduce((acc, manifest) => {
            // Each generator defines the entrypoints they want to generate. Could be one or multiple entrypoints per manifest.
            const entrypointDefinitions = getGenerator(manifest.manifest.type).getEntrypointsDefinition(
                manifest.manifest
            );

            return {
                ...acc,
                ...entrypointDefinitions,
            };
        }, {});
    }

    public generateContextMap() {
        return this.manifests.reduce((acc, manifest) => {
            const entrypointContextMap = getGenerator(manifest.manifest.type).getContextMap(manifest.manifest);

            return {
                ...acc,
                ...entrypointContextMap,
            };
        }, {});
    }

    public apply(compiler: Compiler) {
        const wrmOptions = this.getWrmWebpackPluginOptions(compiler);

        const webfragmentOptions: IWebfragmentsOptions[] = this.manifests.map(manifestDescriptor => {
            const { file: manifestPath, manifest } = manifestDescriptor;
            const { pluginKey } = wrmOptions;

            return {
                manifest,
                manifestPath,
                pluginKey,
            };
        });

        // Each Generator generates the content of their entrypoints.
        const generatedEntrypointsContent = this.generateEntrypointsContent(webfragmentOptions);

        // Now, compile the Content of each of our Entrypoints (modules) with Webpack
        for (const entrypointContent of generatedEntrypointsContent) {
            new VirtualModulePlugin(entrypointContent).apply(compiler);
        }

        /**
         * The following code is responsible for generating the Webfragments Assets (eg: templates for webpanels)
         * and corresponding XML Descriptor.
         */
        compiler.hooks.emit.tapAsync('Add webfragment-xml and assets', (compilation: any, done: () => void) => {
            const webfragmentAssets: IWebfragmentAsset[] = [];
            const webfragmentXMLDescriptorsByType: { [key: string]: string[] } = {};

            // Get all the info we need to compile the XML Descriptor and the Assets
            for (let _webfragmentOption of webfragmentOptions) {
                const { type } = _webfragmentOption.manifest;
                const generator = getGenerator(type);
                const assets = generator.getWebfragmentsAssets(_webfragmentOption);
                const xmlDescriptor = generator.getWebfragmentsXMLDescriptor(_webfragmentOption, assets);

                // Keep a list of all the Assets to compile them later
                webfragmentAssets.push(...assets);

                // Aggregate XML Descriptors by Type to merge them later
                webfragmentXMLDescriptorsByType[type] = webfragmentXMLDescriptorsByType[type]
                    ? [...webfragmentXMLDescriptorsByType[type], xmlDescriptor]
                    : [xmlDescriptor];
            }

            this.compileXMLDescriptor(webfragmentXMLDescriptorsByType, wrmOptions, compiler, compilation);

            this.compileAssets(webfragmentAssets, wrmOptions, compilation);

            done();
        });
    }

    private getWrmWebpackPluginOptions(compiler: Compiler): IWrmPluginOptions {
        const plugins = compiler.options.plugins;
        if (!plugins) {
            throw new Error(`Something went mighty wrong. Can't find any plugins in webpack`);
        }

        const wrmPlugin = plugins.find(p => p.constructor.name === 'WrmPlugin');

        if (!wrmPlugin) {
            throw new Error(
                `Can't find the WrmPlugin. Make sure you are using this plugin only in conjunction with the atlassian-webresource-webpack-plugin`
            );
        }

        return (wrmPlugin as Plugin & { options: IWrmPluginOptions }).options;
    }

    private generateEntrypointsContent(webfragmentOptions: IWebfragmentsOptions[]): IVirtualModuleParams[] {
        // For each manifest (already normalized as Options)...
        return webfragmentOptions.reduce((entrypointsContent, _webfragmentOption) => {
            const { manifest } = _webfragmentOption;
            const generator = getGenerator(manifest.type);

            //.. Set the Content of each Entrypoint already specified in 'generateEntrypoints'
            return [...entrypointsContent, ...generator.getEntrypointsContent(_webfragmentOption)];
        }, []);
    }

    private compileXMLDescriptor(
        xmlDescriptorsByType: { [key: string]: string[] },
        wrmOptions: IWrmPluginOptions,
        compiler: any,
        compilation: any
    ) {
        // Merge all XML descriptors into one huge XML string to be compile later.
        const xmlDescriptorContent = PrettyData.xml(
            Object.entries(xmlDescriptorsByType)
                .map(([type, xmlDescriptors]) => {
                    return getGenerator(type).combineWebfragmentsXMLDescriptors(xmlDescriptors);
                })
                .join('\n')
        );

        const xmlDescriptorDirname = path.dirname(
            path.relative((compiler.options.output as Output).path || '', wrmOptions.xmlDescriptors)
        );
        const webfragmentXmlDescriptorFilename = path.join(xmlDescriptorDirname, 'wr-generated-webfragments.xml');

        // do not generate xml descriptor if there is nothing to add
        if (!xmlDescriptorContent) {
            return;
        }

        // Compile the XML Descriptor File
        compilation.assets[webfragmentXmlDescriptorFilename] = {
            source: () => new Buffer(xmlDescriptorContent),
            size: () => Buffer.byteLength(xmlDescriptorContent),
        };
    }

    private compileAssets(webfragmentAssets: IWebfragmentAsset[], wrmOptions: IWrmPluginOptions, compilation: any) {
        // Get the location where we're gonna put the files
        const locationPrefix = (wrmOptions.locationPrefix || '').replace(/^\//, '');

        // Compile all assets for each webfragment
        for (const asset of webfragmentAssets) {
            const assetPath = path.join(locationPrefix, asset.path);

            compilation.assets[assetPath] = {
                source: () => new Buffer(asset.content),
                size: () => Buffer.byteLength(asset.content),
            };
        }
    }
};
